package zen_training_java_reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) {
		
		Model model = (Model) Model.getInstance(Model.class, "md Name");
		System.out.println("Reflective class generation");
		System.out.println("Model.getValue() result: " + model.getValue());
		
		ChildModel childModel = (ChildModel) ChildModel.getInstance(ChildModel.class, "child md Name");
		System.out.println("ChildModel.getValue() result: " + childModel.getValue());
		System.out.println("ChildModel.getValue_2() result: " + childModel.getValue_2());
		System.out.println();
		
		Class[] paramTypes = new Class[] { }; 
		try {
			Method method = model.getClass().getMethod("getValue", paramTypes);
			String result = (String) method.invoke(model, paramTypes);
			System.out.println("Reflective invoke model.getValue(): ");
			System.out.println("Result: " + result);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
