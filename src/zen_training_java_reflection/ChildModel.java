package zen_training_java_reflection;

public class ChildModel extends Model{
	public String value_2 = "val2";
	
	public ChildModel(String value) {
		super(value);
	}

	public String getValue_2() {
		return value_2;
	}
}
