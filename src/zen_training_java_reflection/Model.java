package zen_training_java_reflection;

import java.lang.reflect.InvocationTargetException;

public class Model {
	private String mValue;
	final static public String CLASS_NAME = "zen_training_java_reflection.Model";
	
	public Model(String value) {
		mValue = value;
	}
	
	public String getValue() {
		return mValue;
	}

	public static Object getInstance(Class c, String value) {
		Object object = null;
		
		try {
			object = c.getDeclaredConstructor(String.class).newInstance(value);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	return object;
	}
}
